import React from 'react';
import './App.css';
import logo from './logo.svg';
import MessageBox from './components/MessageBox';

function App() {
  return (
    <>
      <header>
        <img src={logo} className="app-logo" alt="logo"/>
      </header>
      <div className="container">
        <div className="row columns">
          <div className="column">
            <MessageBox name="User 1" />
          </div>
          <div className="column">
            <MessageBox name="User 2" />
          </div>
        </div>

        <div className="row">
          Thanks for using CollectiveHealthq Chat 💬.
        </div>
      </div>
    </>
  );
}

export default App;
