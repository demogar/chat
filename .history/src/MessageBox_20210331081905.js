import React from 'react';

const MessageBox = ({ name }) => {
  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages"></div>
      <textarea></textarea>
      <button>Send Message</button>
    </div>
  );
};

export default MessageBox;
