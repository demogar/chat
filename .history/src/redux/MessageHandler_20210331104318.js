import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [],
};

export const messageSlices = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addSingleMessage: (state, action) => {
      state = {
        ...state,
        data: state.data.push(action.payload)
      }
    },
    removeSingleMessage: (state, action) => {
      debugger;
    },
  },
});

export const { addSingleMessage, removeSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.data;
export default messageSlices.reducer;
