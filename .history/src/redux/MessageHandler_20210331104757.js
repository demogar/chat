import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [],
};

export const messageSlices = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addSingleMessage: (state, action) => {
      state = {
        ...state,
        data: state.data.push(action.payload)
      }
    },
    removeSingleMessage: (state, action) => {
      const { messageId } = action.payload;

      // Get all the messages ids
      var messageIndex = state.data.map(message => {
        return message.id;
      }).indexOf(messageId);

      // If the message id exists, remove it
      if (messageIndex >= 0) {
        state = {
          ...state,
          data: state.data.splice(messageIndex, 1),
        };
      }
    },
  },
});

export const { addSingleMessage, removeSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.data;
export default messageSlices.reducer;
