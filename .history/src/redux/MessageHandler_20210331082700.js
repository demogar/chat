import { createSlice } from '@reduxjs/toolkit';

export const messageSlices = createSlice({
  name: 'messages',
  initialState: {
    data: [],
  },
  reducers: {
    addSingleMessage: (state, message) => {
      state = {
        ...state,
        data: state.data.push(message)
      }
    }
  },
});

export const { addSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.data;
export default messageSlices.reducer;
