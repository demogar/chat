import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: [
    { from: "user 1", body: "asdasd" }
  ],
};

export const messageSlices = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addSingleMessage: (state, message) => {
      state = {
        ...state,
        data: state.data.push(message)
      }
    }
  },
});

export const { addSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.data;
export default messageSlices.reducer;
