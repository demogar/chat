import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  messages: [
    { from: "user 1", body: "asdasd" }
  ],
};

export const messageSlices = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addSingleMessage: (state, message) => {
      state = {
        ...state,
        messages: state.messages.push(message)
      }
    }
  },
});

export const { addSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.messages;
export default messageSlices.reducer;
