import { createSlice } from '@reduxjs/toolkit';

export const messageSlices = createSlice({
  name: 'messages',
  initialState: {
    data: [],
  },
  reducers: {
    addMessage: (state, message) => {
      state = {
        ...state,
        data: state.data.push(message)
      }
    }
  },
});

export const { addMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.data;
export default messageSlices.reducer;
