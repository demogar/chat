import { createSlice } from '@reduxjs/toolkit';

export const messageSlices = createSlice({
  name: 'messages',
  initialState: {
    messages: [],
  },
  reducers: {
    addMessage: (state, message) => {
      state = {
        ...state,
        messages: state.messages.push(message)
      }
    }
  },
});

export const { addMessage } = messageSlices.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectMessages = state => state.messages.data;

export default messageSlices.reducer;
