import { createSlice } from '@reduxjs/toolkit';

export const messageSlices = createSlice({
  name: 'messages',
  initialState: {
    messages: [],
  },
  reducers: {
    addSingleMessage: (state, message) => {
      state = {
        ...state,
        messages: state.data.push(message)
      }
    }
  },
});

export const { addSingleMessage } = messageSlices.actions;
export const selectMessages = state => state.messages.messages;
export default messageSlices.reducer;
