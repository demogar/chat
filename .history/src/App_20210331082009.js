import React from 'react';
import { Counter } from './features/counter/Counter';
import './App.css';
import MessageBox from './MessageBox';

function App() {
  return (
    <div className="container">
      <div className="row columns">
        <Counter />
        <div className="column">
          <MessageBox name="User 1" />
        </div>
        <div className="column">
          <MessageBox name="User 2" />
        </div>
      </div>

      <div className="row">
        c
      </div>
    </div>
  );
}

export default App;
