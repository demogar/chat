import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSingleMessage,
  selectMessages,
} from './redux/MessageHandler';

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [messages, setMessages] = useState([]);

  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages">
        {JSON.stringify(data)}
      </div>
      <textarea></textarea>
      <button>Send Message</button>
    </div>
  );
};

export default MessageBox;
