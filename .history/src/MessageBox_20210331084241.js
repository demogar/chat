import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSingleMessage,
  selectMessages,
} from './redux/MessageHandler';

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [newMessage, setNewMessage] = useState("");
  const addMessageToBox = (messageToAdd) => {
    debugger;
  }

  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages">
        {JSON.stringify(data)}
      </div>
      <textarea onChange={event => setNewMessage(event.target.value)}>{newMessage}</textarea>
      <button onClick={() => addMessageToBox(newMessage)}>Send Message</button>
    </div>
  );
};

export default MessageBox;
