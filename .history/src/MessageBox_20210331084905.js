import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSingleMessage,
  selectMessages,
} from './redux/MessageHandler';

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [newMessage, setNewMessage] = useState("");
  const addMessageToBox = (messageToAdd) => {
    if (messageToAdd.trim() === "") return;

    setNewMessage("");

    const messageObject = {
      from: name,
      body: messageToAdd,
    };

    dispatch(addSingleMessage(messageObject));
  }

  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages">
        {JSON.stringify(data)}
      </div>
      <input onChange={event => setNewMessage(event.target.value)} value={newMessage} />
      <button onClick={() => addMessageToBox(newMessage)}>Send Message</button>
    </div>
  );
};

export default MessageBox;
