import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

const MessageBox = ({ name }) => {
  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages"></div>
      <textarea></textarea>
      <button>Send Message</button>
    </div>
  );
};

export default MessageBox;
