import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addMessage,
  selectMessages,
} from './redux/MessageHandler';

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [addSingleMessage, setIncrementAmount] = useState([]);

  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages"></div>
      <textarea></textarea>
      <button>Send Message</button>
    </div>
  );
};

export default MessageBox;
