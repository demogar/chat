import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <span>User: {message.user}</span>
      <span>{message.body}</span>
    </div>
  );
};

export default SingleMessageReceived;
