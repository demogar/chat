import React from 'react';
import SingleMessageReceived from './SingleMessageReceived';

const MessagesReceived = ({ messages }) => {
  if (messages.length === 0) {
    return (
      <div>
        <p>Welcome to CollectiveHealth Chat 👋👋.</p>
      </div>
    );
  }

  return messages.map((message) => <SingleMessageReceived message={message} />);
};

export default MessagesReceived;
