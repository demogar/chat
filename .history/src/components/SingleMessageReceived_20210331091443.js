import React from 'react';

const SingleMessageReceived = ({ message }) => {
  const formatDate = (dateString) => {
    if (dateString.trim()) {
      const date = new Date(dateString);

      return date.toLocaleString();
    }

    return "";
  };

  return (
    <div>
      <h5>{message.from}</h5>
      <p>{formatDate(message.date)}</p>
      <p>{message.body}</p>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
