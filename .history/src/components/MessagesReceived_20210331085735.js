import React from 'react';
import SingleMessageReceived from './SingleMessageReceived';

const MessagesReceived = ({ messages }) => {
  if (messages.length === 0) {
    return (
      <div>Welcome to CollectiveHealth Chat.</div>
    );
  }

  messages.map((message) => <SingleMessageReceived message={message} />);
};

export default MessagesReceived;
