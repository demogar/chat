import PropTypes from 'prop-types';
import React from 'react';

const SingleMessageReceived = ({ message }) => {
  const formatDate = (dateString) => {
    if (dateString.trim()) {
      const date = new Date(dateString);

      return date.toLocaleString();
    }

    return "";
  };

  return (
    <div className="single-message">
      <h5>{message.from}</h5>
      <p className="message-date">{formatDate(message.date)}</p>
      <p>{message.body}</p>
      <hr />
    </div>
  );
};

SingleMessageReceived.prototypes = {
  error: PropTypes.shape({
    from: PropTypes.string.required,
    date: PropTypes.string.required,
    body: PropTypes.string.required,
  }),
};

export default SingleMessageReceived;
