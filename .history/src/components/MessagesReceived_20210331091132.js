import React from 'react';
import SingleMessageReceived from './SingleMessageReceived';
import PropTypes from 'prop-types';

const MessagesReceived = ({ messages }) => {
  if (messages.length === 0) {
    return (
      <div>
        <p>Welcome to CollectiveHealth Chat 👋👋.</p>
      </div>
    );
  }

  return messages.map((message) => <SingleMessageReceived message={message} />);
};

MessagesReceived.prototypes = {
  messages: PropTypes.array,
}

export default MessagesReceived;
