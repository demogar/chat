import React from 'react';
import SingleMessageReceived from './SingleMessageReceived';
import PropTypes from 'prop-types';

/**
 * Checks if the `from` user and the `currentUser` are equal
 *
 * @param {string} userMessage
 * @param {string} currentUser
 * @returns
 */
const isTheSameUser = (userMessage, currentUser) => {
  if (userMessage && currentUser) {
    return userMessage.trim().toUpperCase() === currentUser.trim().toUpperCase();
  }

  return false;
}

const MessagesReceived = ({ messages, currentUser }) => {
  if (messages.length === 0) {
    return (
      <div>
        <p>Welcome to CollectiveHealth Chat 👋👋.</p>
      </div>
    );
  }

  return messages.map((message) => {
    const isCurrentUser = isTheSameUser(message.from, currentUser);

    // We'll construct a unique id (per user column), so that we keep it unique
    // across columns
    return (
      <SingleMessageReceived
        key={`${currentUser}-${message.id}`}
        message={message}
        isCurrentUser={isCurrentUser}
      />
    )
  });
};

MessagesReceived.prototypes = {
  messages: PropTypes.bool,
  user: PropTypes.string.isRequired,
}

export default MessagesReceived;
