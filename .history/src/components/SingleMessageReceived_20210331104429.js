import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch } from 'react-redux';
import {
  removeSingleMessage,
} from '../redux/MessageHandler';

const SingleMessageReceived = ({ message, isCurrentUser }) => {
  const dispatch = useDispatch();
  const formatDate = (dateString) => {
    if (dateString.trim()) {
      const date = new Date(dateString);

      return date.toLocaleString();
    }

    return "";
  };
  const dispatchDeleteMessage = (messageId) => {
    debugger;
    dispatch(removeSingleMessage(messageId));
  }

  return (
    <div className="single-message">
      <h5>{message.from}</h5>
      <p className="message-date">{formatDate(message.date)}</p>
      <p className="message-body">{message.body}</p>
      {isCurrentUser && <p className="message-remove"><button onClick={() => }>Delete Message</button></p>}
      <hr />
    </div>
  );
};

SingleMessageReceived.prototypes = {
  error: PropTypes.shape({
    from: PropTypes.string.required,
    date: PropTypes.string.required,
    body: PropTypes.string.required,
  }),
  isCurrentUser: PropTypes.bool.isRequired,
};

export default SingleMessageReceived;
