import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <span>User: {message.from}</span>
      <span>{message.body}</span>
    </div>
  );
};

export default SingleMessageReceived;
