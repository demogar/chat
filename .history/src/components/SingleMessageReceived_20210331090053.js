import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <strong>{message.from}</strong>
      <em>{message.body}</em>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
