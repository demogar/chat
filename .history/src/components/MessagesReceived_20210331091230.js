import React from 'react';
import SingleMessageReceived from './SingleMessageReceived';
import PropTypes from 'prop-types';

const MessagesReceived = ({ messages }) => {
  if (messages.length === 0) {
    return (
      <div>
        <p>Welcome to CollectiveHealth Chat 👋👋.</p>
      </div>
    );
  }

  return messages.map((message) => <SingleMessageReceived key={message.id} message={message} />);
};

MessagesReceived.prototypes = {
  messages: PropTypes.bool,
}

export default MessagesReceived;
