import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <h5>{message.from}</h5>
      <p>{message.body}</p>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
