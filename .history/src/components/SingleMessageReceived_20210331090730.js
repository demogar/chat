import React from 'react';

const SingleMessageReceived = ({ message }) => {
  const formatDate = (date) => {
    if (date) return date.toLocaleDateString();
    return "";
  };

  return (
    <div>
      <h5>{message.from}</h5>
      <p>{formatDate(message.date)}</p>
      <p>{message.body}</p>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
