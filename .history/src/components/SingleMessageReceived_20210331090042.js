import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <span>User: {message.from}</span>
      <em>{message.body}</em>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
