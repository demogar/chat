import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSingleMessage,
  selectMessages,
} from '../redux/MessageHandler';
import MessagesReceived from './MessagesReceived';

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [newMessage, setNewMessage] = useState("");
  const addMessageToBox = (messageToAdd) => {
    // Check for message existence
    if (messageToAdd.trim() === "") return;

    // Clear the input/textarea field
    setNewMessage("");

    // Add new message to the messages box
    const messageObject = {
      from: name,
      body: messageToAdd,
    };

    dispatch(addSingleMessage(messageObject));
  };

  return (
    <div>
      <div className="name">{name}</div>
      <div className="messages">
        <MessagesReceived messages={data} />
      </div>
      <textarea
        onChange={event => setNewMessage(event.target.value)}
        value={newMessage}
      />
      <button onClick={() => addMessageToBox(newMessage)}>Send Message</button>
    </div>
  );
};

export default MessageBox;
