import React from 'react';

const SingleMessageReceived = ({ message }) => {
  return (
    <div>
      <h1>{message.from}</h1>
      <p>{message.body}</p>
      <hr />
    </div>
  );
};

export default SingleMessageReceived;
