import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  addSingleMessage,
  selectMessages,
} from '../redux/MessageHandler';
import MessagesReceived from './MessagesReceived';

const replaceSpaceWithUnderscore = (text) => {
  if (!text) return "";

  return text.replace(/ /g,"_");
}

const MessageBox = ({ name }) => {
  const data = useSelector(selectMessages);
  const dispatch = useDispatch();
  const [newMessage, setNewMessage] = useState("");
  const addMessageToBox = (messageToAdd) => {
    // Check for message existence
    if (messageToAdd.trim() === "") return;

    // Clear the input/textarea field
    setNewMessage("");

    // Add new message to the messages box
    const messageObject = {
      id: Math.floor(Math.random() * 100),
      from: name,
      body: messageToAdd,
      // Adding it as a non-serializable parameter
      // https://redux.js.org/style-guide/style-guide#do-not-put-non-serializable-values-in-state-or-actions
      date: `${new Date()}`,
      isRemoved: false,
    };

    dispatch(addSingleMessage(messageObject));
  };

  return (
    <div className="message-box">
      <div className="name"><h2>{name}</h2></div>
      <div className="messages">
        <MessagesReceived messages={data} currentUser={name} />
      </div>
      <textarea
        id={`message-box-${replaceSpaceWithUnderscore(name)}`}
        onChange={event => setNewMessage(event.target.value)}
        value={newMessage}
      />
      <button
        id={`message-button-${replaceSpaceWithUnderscore(name)}`}
        onClick={() => addMessageToBox(newMessage)}
      >
        Send Message
      </button>
    </div>
  );
};

export default MessageBox;
