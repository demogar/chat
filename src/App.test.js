import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './app/store';
import App from './App';

/**
 * Helper method to add a new message
 *
 * @param {*} component
 * @param {*} message
 */
const addNewMessage = (component, message) => {
  const { queryAllByRole, getAllByText } = component;
  const textboxes = queryAllByRole("textbox");
  const buttons = queryAllByRole("button");

  const user1Textbox = textboxes.find(textarea => textarea.id === "message-box-User_1");
  const user1Button = buttons.find(button => button.id === "message-button-User_1");

  // Change the message box text
  fireEvent.change(user1Textbox, { target: { value: message } });
  expect(user1Textbox.value).toBe(message);

  // Add the new message by clicking on the button
  fireEvent.click(user1Button);

  // Message box was cleared
  expect(user1Textbox.value).toBe("");

  // New message was added
  expect(getAllByText(message)).toBeTruthy();
};

/**
 * Integration tests for the CHat Application
 */
describe("<App /> component", () => {
  let component;

  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <App />
      </Provider>
    );
  });

  test('renders the expected components', () => {
    const { getByText, getAllByText, getAllByRole } = component;

    expect(getByText(/Thanks for using CollectiveHealth Chat/i)).toBeInTheDocument();

    // The user 1 and user 2
    expect(getAllByText("User 1")).toBeTruthy();
    expect(getAllByText("User 2")).toBeTruthy();

    // Renders the textboxes and buttons
    expect(getAllByRole("textbox")).toBeTruthy();
    expect(getAllByRole("button")).toBeTruthy();
  });

  test("a new message can be added", () => {
    addNewMessage(component, "New Message 1");
    addNewMessage(component, "New Message 2");
  });

  test("a message can be deleted", () => {
    const { queryAllByRole, getAllByText, queryByText } = component;

    addNewMessage(component, "New Message 3");
    expect(queryByText("Deleted ☠")).toBeFalsy();

    const buttons = queryAllByRole("button");
    const deleteButton = buttons.find(button => button.className === "delete-single-message");
    fireEvent.click(deleteButton);

    expect(getAllByText("Deleted ☠")).toBeTruthy();
  });
});
