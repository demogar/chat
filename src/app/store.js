import { configureStore } from '@reduxjs/toolkit';
import messagesReducer from '../redux/MessageHandler';

export default configureStore({
  reducer: {
    messages: messagesReducer,
  },
});
